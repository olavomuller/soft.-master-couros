﻿using MasterCouros.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MasterCouros
{
   
    public partial class Form1 : Form
    {
        private clsPessoa admpessoa;
        public Form1()
        {
            InitializeComponent();
            admpessoa = new clsPessoa();
            
        }
       

        private void button1_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            admpessoa.nome = textBox1.Text;
            admpessoa.InserirPessoa();
            MessageBox.Show("Pessoa Adicionada com sucesso!");
            Cursor = Cursors.Default;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            dataGridView1.Rows.Clear();
            admpessoa.ListaPessoas();
            
            for(int i = 0; i <= admpessoa.MinhaLista.Items.Count - 1; i++)
            {
                dataGridView1.Rows.Add(admpessoa.MinhaLista.Items[i].Text);
               
            }
           Cursor = Cursors.Default;
        }
    }
}
