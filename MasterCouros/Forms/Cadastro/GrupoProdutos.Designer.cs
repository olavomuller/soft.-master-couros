﻿namespace MasterCouros.Forms.Cadastro
{
    partial class GrupoProdutos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GrupoProdutos));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gpCamposGrupos = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btnRefresh = new DevComponents.DotNetBar.ButtonX();
            this.cbDesativaGrupo = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.txtDescGrupo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.txtCodGrupo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.gpAcoes = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btnCancelar = new DevComponents.DotNetBar.ButtonX();
            this.btnSalvar = new DevComponents.DotNetBar.ButtonX();
            this.btnEditarGrupo = new DevComponents.DotNetBar.ButtonX();
            this.btnAddGrupo = new DevComponents.DotNetBar.ButtonX();
            this.btnSair = new DevComponents.DotNetBar.ButtonX();
            this.dgGrupoProdutos = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gpCamposGrupos.SuspendLayout();
            this.gpAcoes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgGrupoProdutos)).BeginInit();
            this.SuspendLayout();
            // 
            // gpCamposGrupos
            // 
            this.gpCamposGrupos.CanvasColor = System.Drawing.SystemColors.Control;
            this.gpCamposGrupos.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.gpCamposGrupos.Controls.Add(this.btnRefresh);
            this.gpCamposGrupos.Controls.Add(this.cbDesativaGrupo);
            this.gpCamposGrupos.Controls.Add(this.labelX2);
            this.gpCamposGrupos.Controls.Add(this.txtDescGrupo);
            this.gpCamposGrupos.Controls.Add(this.labelX1);
            this.gpCamposGrupos.Controls.Add(this.txtCodGrupo);
            this.gpCamposGrupos.Dock = System.Windows.Forms.DockStyle.Top;
            this.gpCamposGrupos.Location = new System.Drawing.Point(5, 1);
            this.gpCamposGrupos.Margin = new System.Windows.Forms.Padding(4);
            this.gpCamposGrupos.Name = "gpCamposGrupos";
            this.gpCamposGrupos.Size = new System.Drawing.Size(737, 120);
            // 
            // 
            // 
            this.gpCamposGrupos.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.gpCamposGrupos.Style.BackColorGradientAngle = 90;
            this.gpCamposGrupos.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.gpCamposGrupos.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpCamposGrupos.Style.BorderBottomWidth = 1;
            this.gpCamposGrupos.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.gpCamposGrupos.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpCamposGrupos.Style.BorderLeftWidth = 1;
            this.gpCamposGrupos.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpCamposGrupos.Style.BorderRightWidth = 1;
            this.gpCamposGrupos.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpCamposGrupos.Style.BorderTopWidth = 1;
            this.gpCamposGrupos.Style.CornerDiameter = 4;
            this.gpCamposGrupos.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.gpCamposGrupos.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.gpCamposGrupos.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.gpCamposGrupos.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.gpCamposGrupos.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.gpCamposGrupos.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.gpCamposGrupos.TabIndex = 1;
            this.gpCamposGrupos.Text = "Informações do Grupo de Produtos";
            // 
            // btnRefresh
            // 
            this.btnRefresh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnRefresh.BackColor = System.Drawing.Color.Transparent;
            this.btnRefresh.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue;
            this.btnRefresh.Location = new System.Drawing.Point(24, 70);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(39, 23);
            this.btnRefresh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnRefresh.Symbol = "";
            this.btnRefresh.TabIndex = 5;
            this.btnRefresh.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // cbDesativaGrupo
            // 
            this.cbDesativaGrupo.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.cbDesativaGrupo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.cbDesativaGrupo.Enabled = false;
            this.cbDesativaGrupo.Location = new System.Drawing.Point(585, 70);
            this.cbDesativaGrupo.Name = "cbDesativaGrupo";
            this.cbDesativaGrupo.Size = new System.Drawing.Size(130, 23);
            this.cbDesativaGrupo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbDesativaGrupo.TabIndex = 4;
            this.cbDesativaGrupo.Text = "Desativar Grupo?";
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(121, 8);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(129, 22);
            this.labelX2.TabIndex = 3;
            this.labelX2.Text = "Descrição do Grupo";
            // 
            // txtDescGrupo
            // 
            // 
            // 
            // 
            this.txtDescGrupo.Border.Class = "TextBoxBorder";
            this.txtDescGrupo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDescGrupo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescGrupo.Enabled = false;
            this.txtDescGrupo.Location = new System.Drawing.Point(121, 30);
            this.txtDescGrupo.Margin = new System.Windows.Forms.Padding(4);
            this.txtDescGrupo.Name = "txtDescGrupo";
            this.txtDescGrupo.Size = new System.Drawing.Size(581, 25);
            this.txtDescGrupo.TabIndex = 2;
            this.txtDescGrupo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDescGrupo_Validating);
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(24, 8);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(66, 22);
            this.labelX1.TabIndex = 1;
            this.labelX1.Text = "Código";
            // 
            // txtCodGrupo
            // 
            // 
            // 
            // 
            this.txtCodGrupo.Border.Class = "TextBoxBorder";
            this.txtCodGrupo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtCodGrupo.Enabled = false;
            this.txtCodGrupo.Location = new System.Drawing.Point(24, 30);
            this.txtCodGrupo.Margin = new System.Windows.Forms.Padding(4);
            this.txtCodGrupo.Name = "txtCodGrupo";
            this.txtCodGrupo.Size = new System.Drawing.Size(77, 25);
            this.txtCodGrupo.TabIndex = 0;
            // 
            // gpAcoes
            // 
            this.gpAcoes.CanvasColor = System.Drawing.SystemColors.Control;
            this.gpAcoes.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.gpAcoes.Controls.Add(this.btnCancelar);
            this.gpAcoes.Controls.Add(this.btnSalvar);
            this.gpAcoes.Controls.Add(this.btnEditarGrupo);
            this.gpAcoes.Controls.Add(this.btnAddGrupo);
            this.gpAcoes.Controls.Add(this.btnSair);
            this.gpAcoes.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gpAcoes.Location = new System.Drawing.Point(5, 369);
            this.gpAcoes.Name = "gpAcoes";
            this.gpAcoes.Size = new System.Drawing.Size(737, 52);
            // 
            // 
            // 
            this.gpAcoes.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.gpAcoes.Style.BackColorGradientAngle = 90;
            this.gpAcoes.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.gpAcoes.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpAcoes.Style.BorderBottomWidth = 1;
            this.gpAcoes.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.gpAcoes.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpAcoes.Style.BorderLeftWidth = 1;
            this.gpAcoes.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpAcoes.Style.BorderRightWidth = 1;
            this.gpAcoes.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpAcoes.Style.BorderTopWidth = 1;
            this.gpAcoes.Style.CornerDiameter = 4;
            this.gpAcoes.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.gpAcoes.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.gpAcoes.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.gpAcoes.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.gpAcoes.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.gpAcoes.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.gpAcoes.TabIndex = 2;
            this.gpAcoes.Text = "Ações";
            // 
            // btnCancelar
            // 
            this.btnCancelar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancelar.BackColor = System.Drawing.Color.Transparent;
            this.btnCancelar.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue;
            this.btnCancelar.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnCancelar.Enabled = false;
            this.btnCancelar.Location = new System.Drawing.Point(230, 0);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(91, 26);
            this.btnCancelar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCancelar.Symbol = "";
            this.btnCancelar.TabIndex = 4;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSalvar.BackColor = System.Drawing.Color.Transparent;
            this.btnSalvar.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue;
            this.btnSalvar.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnSalvar.Enabled = false;
            this.btnSalvar.Location = new System.Drawing.Point(144, 0);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(86, 26);
            this.btnSalvar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSalvar.Symbol = "";
            this.btnSalvar.TabIndex = 3;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // btnEditarGrupo
            // 
            this.btnEditarGrupo.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnEditarGrupo.BackColor = System.Drawing.Color.Transparent;
            this.btnEditarGrupo.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue;
            this.btnEditarGrupo.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnEditarGrupo.Enabled = false;
            this.btnEditarGrupo.Location = new System.Drawing.Point(69, 0);
            this.btnEditarGrupo.Name = "btnEditarGrupo";
            this.btnEditarGrupo.Size = new System.Drawing.Size(75, 26);
            this.btnEditarGrupo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnEditarGrupo.Symbol = "";
            this.btnEditarGrupo.TabIndex = 2;
            this.btnEditarGrupo.Text = "Editar";
            this.btnEditarGrupo.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Left;
            this.btnEditarGrupo.Click += new System.EventHandler(this.btnEditarGrupo_Click);
            // 
            // btnAddGrupo
            // 
            this.btnAddGrupo.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddGrupo.BackColor = System.Drawing.Color.Transparent;
            this.btnAddGrupo.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue;
            this.btnAddGrupo.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnAddGrupo.Location = new System.Drawing.Point(0, 0);
            this.btnAddGrupo.Name = "btnAddGrupo";
            this.btnAddGrupo.Size = new System.Drawing.Size(69, 26);
            this.btnAddGrupo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAddGrupo.Symbol = "";
            this.btnAddGrupo.TabIndex = 1;
            this.btnAddGrupo.Text = "Novo";
            this.btnAddGrupo.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Left;
            this.btnAddGrupo.Click += new System.EventHandler(this.btnAddGrupo_Click);
            // 
            // btnSair
            // 
            this.btnSair.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSair.BackColor = System.Drawing.Color.Transparent;
            this.btnSair.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue;
            this.btnSair.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSair.Image = ((System.Drawing.Image)(resources.GetObject("btnSair.Image")));
            this.btnSair.Location = new System.Drawing.Point(663, 0);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(68, 26);
            this.btnSair.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSair.Symbol = "";
            this.btnSair.TabIndex = 0;
            this.btnSair.Text = "Sair";
            this.btnSair.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Left;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // dgGrupoProdutos
            // 
            this.dgGrupoProdutos.AllowUserToAddRows = false;
            this.dgGrupoProdutos.AllowUserToDeleteRows = false;
            this.dgGrupoProdutos.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dgGrupoProdutos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgGrupoProdutos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgGrupoProdutos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgGrupoProdutos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column2,
            this.Column1});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgGrupoProdutos.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgGrupoProdutos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgGrupoProdutos.EnableHeadersVisualStyles = false;
            this.dgGrupoProdutos.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgGrupoProdutos.Location = new System.Drawing.Point(5, 121);
            this.dgGrupoProdutos.MultiSelect = false;
            this.dgGrupoProdutos.Name = "dgGrupoProdutos";
            this.dgGrupoProdutos.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgGrupoProdutos.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgGrupoProdutos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgGrupoProdutos.Size = new System.Drawing.Size(737, 248);
            this.dgGrupoProdutos.TabIndex = 3;
            this.dgGrupoProdutos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgGrupoProdutos_CellClick);
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Código";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 70;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Descrição do Grupo";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 400;
            // 
            // GrupoProdutos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(747, 423);
            this.Controls.Add(this.dgGrupoProdutos);
            this.Controls.Add(this.gpAcoes);
            this.Controls.Add(this.gpCamposGrupos);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "GrupoProdutos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Produtos";
            this.Load += new System.EventHandler(this.Produtos_Load);
            this.gpCamposGrupos.ResumeLayout(false);
            this.gpAcoes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgGrupoProdutos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevComponents.DotNetBar.Controls.GroupPanel gpCamposGrupos;
        private DevComponents.DotNetBar.Controls.CheckBoxX cbDesativaGrupo;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDescGrupo;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.TextBoxX txtCodGrupo;
        private DevComponents.DotNetBar.Controls.GroupPanel gpAcoes;
        private DevComponents.DotNetBar.ButtonX btnSair;
        private DevComponents.DotNetBar.ButtonX btnAddGrupo;
        private DevComponents.DotNetBar.ButtonX btnEditarGrupo;
        private DevComponents.DotNetBar.ButtonX btnSalvar;
        private DevComponents.DotNetBar.ButtonX btnCancelar;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgGrupoProdutos;
        private DevComponents.DotNetBar.ButtonX btnRefresh;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
    }
}