﻿using MasterCouros.Models;
using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace MasterCouros.Forms.Cadastro
{
    public partial class GrupoProdutos : DevComponents.DotNetBar.Office2007RibbonForm
    {
        private clsGrupoProduto admgrupoproduto;

        public GrupoProdutos()
        {
            InitializeComponent();
        }

        private void Produtos_Load(object sender, EventArgs e)
        {
           // Cursor = Cursors.WaitCursor;
           // admgrupoproduto = new clsGrupoProduto();
           //dgGrupoProdutos.Rows.Clear();
           // admgrupoproduto.ListaGrupo();

           // for (int i = 0; i <= admgrupoproduto.DgVirtual.Rows.Count - 1; i++)
           // {
           //     dgGrupoProdutos.Rows.Add(admgrupoproduto.DgVirtual.Rows[i].Cells[1].Value, admgrupoproduto.DgVirtual.Rows[i].Cells[0].Value);

           // }

       
           // if (dgGrupoProdutos.Rows.Count > 0)
           // {
           //     dgGrupoProdutos.CurrentRow.Selected = false;
           // }
           // Cursor = Cursors.Default;
        }


        private void btnSair_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAddGrupo_Click(object sender, EventArgs e)
        {
            //Ao clicar em novo será disparado os comandos abaixo:
            Cursor = Cursors.WaitCursor;
            if(dgGrupoProdutos.Rows.Count > 0)
            {
                dgGrupoProdutos.
                    Rows.Clear();

            }
            txtDescGrupo.Enabled = true;
            btnSalvar.Enabled = true;
            btnCancelar.Enabled = true;
            btnAddGrupo.Enabled = false;
            txtDescGrupo.Focus();
            Cursor = Cursors.Default;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            //Ao clicar em cancelar será disparado os comandos abaixo:
            if (dgGrupoProdutos.Rows.Count > 0)
            {
                dgGrupoProdutos.
                    Rows.Clear();

            }
            Cursor = Cursors.WaitCursor;
            btnAddGrupo.Enabled = true;
            txtCodGrupo.Text = string.Empty;
            txtDescGrupo.Text = string.Empty;
            txtDescGrupo.Enabled = false;
            btnEditarGrupo.Enabled = false;
            btnSalvar.Enabled = false;
            btnCancelar.Enabled = false;
            dgGrupoProdutos.Enabled = true;
            btnAddGrupo.Focus();
            Cursor = Cursors.Default;
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if(txtDescGrupo.Text == string.Empty)
            {
                MessageBox.Show("Informe a descrição do grupo a ser adicionada!", "Campo inválido!", MessageBoxButtons.OK, MessageBoxIcon.Error );
                txtDescGrupo.Focus();
                Cursor = Cursors.Default;
                return;
                
            }
            if(txtCodGrupo.Text == string.Empty)
            {
                admgrupoproduto = new clsGrupoProduto()
                {
                    DescGrupo = txtDescGrupo.Text
                };
                admgrupoproduto.InserirGrupo();
                MessageBox.Show("Grupo de Produto Salvo com Sucesso!", "Mensagem de Registro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDescGrupo.Text = string.Empty;
                Cursor = Cursors.Default;
                return;
            }
            else
            {
                admgrupoproduto = new clsGrupoProduto();
                admgrupoproduto.DescGrupo = txtDescGrupo.Text;
                    if (cbDesativaGrupo.Checked == true)
                 {
                    admgrupoproduto.DesativaGrupo = "S"; 
                 }
                    else
                {
                    admgrupoproduto.DesativaGrupo = null; 
                }

                admgrupoproduto.CodGrupoProduto = Convert.ToInt32(txtCodGrupo.Text);

                admgrupoproduto.AlterarGrupo();
                MessageBox.Show("Grupo de Produto Alterado com Sucesso!", "Mensagem de Atualização", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDescGrupo.Text = string.Empty;
                txtCodGrupo.Text = string.Empty;
                cbDesativaGrupo.Checked = false;
                Cursor = Cursors.Default;
                return;
            }
            
            



        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            admgrupoproduto = new clsGrupoProduto();
            dgGrupoProdutos.Rows.Clear();
            admgrupoproduto.ListaGrupo();

            for (int i = 0; i <= admgrupoproduto.DgVirtual.Rows.Count - 1; i++)
            {
                dgGrupoProdutos.Rows.Add(admgrupoproduto.DgVirtual.Rows[i].Cells[1].Value, admgrupoproduto.DgVirtual.Rows[i].Cells[0].Value);

            }


            if (dgGrupoProdutos.Rows.Count > 0)
            {
                dgGrupoProdutos.CurrentRow.Selected = false;
            }
            Cursor = Cursors.Default;
        }

        private void dgGrupoProdutos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if (this.dgGrupoProdutos.CurrentRow.Selected == true)
            {
                btnAddGrupo.Enabled = false;
                btnSalvar.Enabled = false;
                btnEditarGrupo.Enabled = true;
                btnCancelar.Enabled = true;


            }
            Cursor = Cursors.Default;

        }

        private void btnEditarGrupo_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if (this.dgGrupoProdutos.CurrentRow.Selected == true)
            {
                txtCodGrupo.Text = this.dgGrupoProdutos.CurrentRow.Cells[0].Value.ToString();
                txtDescGrupo.Text = this.dgGrupoProdutos.CurrentRow.Cells[1].Value.ToString();
                txtDescGrupo.Enabled = true;
                cbDesativaGrupo.Enabled = true;
                btnAddGrupo.Enabled = false;
                btnEditarGrupo.Enabled = false;
                btnCancelar.Enabled = true;
                btnSalvar.Enabled = true;
                dgGrupoProdutos.Enabled = false;
                txtDescGrupo.Focus();


            }
            Cursor = Cursors.Default;
        }

        private void txtDescGrupo_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
           
        }
    }
}
