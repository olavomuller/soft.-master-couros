﻿using MasterCouros.Models;
using System;
using System.Windows.Forms;

namespace MasterCouros.Forms.Cadastro
{
    public partial class Produto : DevComponents.DotNetBar.Office2007RibbonForm
    {
        private clsGrupoProduto admgrupoproduto;
        private clsProduto admproduto;
        private string _destino;
        private string _controlEstoque;
        private bool BoolControlaEstoque;
        private string _bloqueiaCompra;
        private bool BoolBloqueiaCompra;

        public Produto()
        {
            InitializeComponent();
        }

        private void Produto_Load(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            try
            {
                admgrupoproduto = new clsGrupoProduto();
                admgrupoproduto.ListaGrupo();
                txtGrupo.Items.Clear();
            for (int i = 0; i <= admgrupoproduto.DgVirtual.Rows.Count - 1; i++)
            {
                txtGrupo.Items.Add(admgrupoproduto.DgVirtual.Rows[i].Cells[1].Value + "- " + admgrupoproduto.DgVirtual.Rows[i].Cells[0].Value);
            }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Erro ao recuperar grupo de produtos: " + ex.Message);
            }

            Cursor = Cursors.Default;
        }

        private void txtGrupo_Click(object sender, EventArgs e)
        {
           
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            Close();
            Cursor = Cursors.Default;
        }

        private void txtDestino_SelectedValueChanged(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if(txtDestino.Text == "COMPRA")
            {
                txtUnidCompra.Enabled = true;
                txtUnidVenda.Enabled = false;
                txtUnidCompra.Focus();
                cbControlaEstoque.Enabled = true;
                cbBloqueiaCompra.Enabled = true;
                Cursor = Cursors.Default;
                return;
            }

            if(txtDestino.Text == "VENDA")
            {
                txtUnidCompra.Enabled = false;
                txtUnidVenda.Enabled = true;
                txtUnidVenda.Focus();
                cbControlaEstoque.Enabled = true;
                cbBloqueiaCompra.Enabled = true;
                Cursor = Cursors.Default;
                return;
            }

            if(txtDestino.Text == "AMBOS")
            {
                txtUnidCompra.Enabled = true;
                txtUnidVenda.Enabled = true;
                txtUnidCompra.Focus();
                cbControlaEstoque.Enabled = true;
                cbBloqueiaCompra.Enabled = true;
                Cursor = Cursors.Default;
                return;
            }
           
        }

        private void lblPesquisaProduto_Click(object sender, EventArgs e)
        {

        }

        private void btnPesquisar_CheckedChanged(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if(btnPesquisar.Checked == true)
            {
                lblPesquisaProduto.Visible = true;
                txtPesquisaProduto.Visible = true;
                txtPesquisaProduto.Text = string.Empty;
                txtPesquisaProduto.Focus();
            }
            else
            {
                lblPesquisaProduto.Visible = false;
                txtPesquisaProduto.Visible = false;
                txtPesquisaProduto.Text = string.Empty;
                dgProduto.Rows.Clear();
            }
            Cursor = Cursors.Default;
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            //INICIA O COMANDO COM O CURSOR EM ESPERA
            Cursor = Cursors.WaitCursor;

           if(btnSalvar.Text == "Salvar")
            { 


            //CRIA A CONDIÇÃO SE O CAMPO DESCRIÇÃO FOR VAZIO
            if (txtDescProduto.Text == string.Empty)
            {
                //SE O CAMPO ESTIVER MESMO VAZIO ELE INPUTARÁ A MENSAGEM 
                MessageBox.Show("O Campo descrição é obrigatório!", "Campo inválido", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //FOCA NO CAMPO DE TEXTO VAZIO
                txtDescProduto.Focus();
                //COLOCA O CURSOR EM SEU ESTADO NORMAL
                Cursor = Cursors.Default;
                //RETORNA PARA O ESTADO NORMAL(PARA A EXECUÇÃO DO EVENTO)
                return;
            }

            if (txtGrupo.Text == string.Empty)
            {
                MessageBox.Show("O campo Grupo é obrigatório!", "Campo inválido", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtGrupo.Focus();
                Cursor = Cursors.Default;
                return;
            }

            if (txtDestino.Text == string.Empty)
            {
                MessageBox.Show("O campo Destino é obrigatório!", "Campo inválido", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtDestino.Focus();
                Cursor = Cursors.Default;
                return;
            }

            //SE AS CONDIÇÕES ACIMA NAO FOREM SATISFEITAS É SINAL QUE OS CAMPOS OBRIGATÓRIOS FORAM PREENCHIDOS
            //CODIFICAR A STRING DESTINO PARA GRAVAR APENAS UM CHAR
           
         switch(txtDestino.Text)
            {
                case "COMPRA":
                    _destino = "C";
                    break;
                case "VENDA":
                    _destino = "V";
                    break;
                case "AMBOS":
                    _destino = "A";
                    break;

            }

            //VERIFICA SE O CAMPO PARA CONTROLAR ESTOQUE FOI MARCADO
            if(cbControlaEstoque.Checked == true)
            {
                _controlEstoque = "S";
            }
            else
            {
                _controlEstoque = "N";
            }

            //VERIFICA SE O CAMPO BLOQUEIA COMPRA FOI MARCADO
            if(cbBloqueiaCompra.Checked == true)
            {
                _bloqueiaCompra = "S";
            }
            else
            {
                _bloqueiaCompra = "N";
            }


            //EXECUTAR O INSERT CHAMANDO O METODO DA CLASSE PRODUTO
            try
            {
                admproduto = new clsProduto()
                {
                    DescProduto = txtDescProduto.Text,
                    CodGrupo = Convert.ToInt32(string.Join("", System.Text.RegularExpressions.Regex.Split(txtGrupo.Text, @"[^\d]"))),
                    Destino = _destino,
                    UnidCompra = txtUnidCompra.Text,
                    UnidVenda = txtUnidVenda.Text,
                    ControlaEstoque = _controlEstoque,
                    BloqueiaCompra = _bloqueiaCompra,
                    BloqueiaProduto = null,
                    Obs = txtObs.Text


                };
                admproduto.InserirProduto();
                MessageBox.Show("Produto foi Inserido com Sucesso!", "Mensagem do Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDescProduto.Text = string.Empty;
                txtGrupo.Text = string.Empty;
                txtDestino.Text = string.Empty;
                txtUnidCompra.Text = string.Empty;
                txtUnidCompra.Enabled = false;
                txtUnidVenda.Text = string.Empty;
                txtUnidVenda.Enabled = false;
                cbControlaEstoque.Checked = false;
                cbBloqueiaCompra.Checked = false;
                txtObs.Text = string.Empty;
                Cursor = Cursors.Default;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Erro de Execução do Comando!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



                return;
            }
            
            if (btnSalvar.Text == "Alterar")
            {

            }

        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            try
            {
                admgrupoproduto = new clsGrupoProduto();
                admgrupoproduto.ListaGrupo();
                txtGrupo.Items.Clear();
                for (int i = 0; i <= admgrupoproduto.DgVirtual.Rows.Count - 1; i++)
                {
                    txtGrupo.Items.Add(admgrupoproduto.DgVirtual.Rows[i].Cells[1].Value + "- " + admgrupoproduto.DgVirtual.Rows[i].Cells[0].Value);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao recuperar grupo de produtos: " + ex.Message);
            }

            Cursor = Cursors.Default;
        }

        private void btnRefreshGrid_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            admproduto = new clsProduto();
            dgProduto.Rows.Clear();
            admproduto.ListaProdutos();

            for (int i = 0; i <= admproduto.DgVirtual.Rows.Count - 1; i++)
            {
                dgProduto.Rows.Add(admproduto.DgVirtual.Rows[i].Cells[0].Value, admproduto.DgVirtual.Rows[i].Cells[1].Value,
                    admproduto.DgVirtual.Rows[i].Cells[2].Value, admproduto.DgVirtual.Rows[i].Cells[3].Value,
                    admproduto.DgVirtual.Rows[i].Cells[4].Value, admproduto.DgVirtual.Rows[i].Cells[5].Value,
                    admproduto.DgVirtual.Rows[i].Cells[6].Value, admproduto.DgVirtual.Rows[i].Cells[7].Value,
                    admproduto.DgVirtual.Rows[i].Cells[8].Value, admproduto.DgVirtual.Rows[i].Cells[9].Value);

            }


            if (dgProduto.Rows.Count > 0)
            {
                dgProduto.CurrentRow.Selected = false;
            }
            Cursor = Cursors.Default;
        }

        private void dgProduto_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            
            if(dgProduto.CurrentRow.Selected == true)
            {
                //DECODIFICA O DESTINO
                switch (dgProduto.CurrentRow.Cells[8].Value.ToString())
                {
                    case "C":
                        _destino = "COMPRA";
                        break;
                    case "V":
                        _destino = "VENDA";
                        break;
                    case "A":
                        _destino = "AMBOS";
                        break;

                }

                //DECODIFICA O CONTROLA ESTOQUE

                switch (dgProduto.CurrentRow.Cells[3].Value.ToString())
                {
                    case "S":
                        BoolControlaEstoque = true;
                        break;
                    case "N":
                        BoolControlaEstoque = false;
                        break;
                }

                //DECODIFICA O BLOQUEIO DE COMPRA

                switch (dgProduto.CurrentRow.Cells[2].Value.ToString())
                {
                    case "S":
                        BoolBloqueiaCompra = true;
                        break;
                    case "N":
                        BoolBloqueiaCompra = false;
                        break;
                }



                txtCodProduto.Text = dgProduto.CurrentRow.Cells[0].Value.ToString();
                txtDescProduto.Text = dgProduto.CurrentRow.Cells[1].Value.ToString();
                txtGrupo.Text = dgProduto.CurrentRow.Cells[4].Value.ToString() + "- " + dgProduto.CurrentRow.Cells[5].Value.ToString();
                txtDestino.Text = _destino;
                txtUnidCompra.Text = dgProduto.CurrentRow.Cells[6].Value.ToString();
                txtUnidVenda.Text = dgProduto.CurrentRow.Cells[7].Value.ToString();
                cbControlaEstoque.Checked = BoolControlaEstoque;
                txtObs.Text = dgProduto.CurrentRow.Cells[9].Value.ToString();
                btnSalvar.Text = "Alterar";
                txtDescProduto.Focus();

            }
            Cursor = Cursors.Default;
        }

        private void txtPesquisaProduto_TextChanged(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            try
                 
            {
                dgProduto.Rows.Clear();
                admproduto = new clsProduto()
                {
                    DescProduto = txtPesquisaProduto.Text
                };
                
               
                admproduto.ListaProdutosByDescricao();

                for (int i = 0; i <= admproduto.DgVirtual.Rows.Count - 1; i++)
                {
                    dgProduto.Rows.Add(admproduto.DgVirtual.Rows[i].Cells[0].Value, admproduto.DgVirtual.Rows[i].Cells[1].Value,
                        admproduto.DgVirtual.Rows[i].Cells[2].Value, admproduto.DgVirtual.Rows[i].Cells[3].Value,
                        admproduto.DgVirtual.Rows[i].Cells[4].Value, admproduto.DgVirtual.Rows[i].Cells[5].Value,
                        admproduto.DgVirtual.Rows[i].Cells[6].Value, admproduto.DgVirtual.Rows[i].Cells[7].Value,
                        admproduto.DgVirtual.Rows[i].Cells[8].Value, admproduto.DgVirtual.Rows[i].Cells[9].Value);

                }


                if (dgProduto.Rows.Count > 0)
                {
                    dgProduto.CurrentRow.Selected = false;
                }

            }
            catch(Exception ex)
            {
                MessageBox.Show("Erro ao recuperar dados de produtos, motivos: " + ex.Message);
            }
            Cursor = Cursors.Default;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            try
            {
                txtCodProduto.Text = string.Empty;
                txtDescProduto.Text = string.Empty;
                txtGrupo.Text = string.Empty;
                txtDestino.Text = string.Empty;
                txtUnidCompra.Text = string.Empty;
                txtUnidCompra.Enabled = false;
                txtUnidVenda.Text = string.Empty;
                txtUnidVenda.Enabled = false;
                cbControlaEstoque.Checked = false;
                cbBloqueiaCompra.Checked = false;
                txtObs.Text = string.Empty;
                btnSalvar.Text = "Salvar";
                dgProduto.Rows.Clear();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message);
            }
            Cursor = Cursors.Default;
        }

        private void TbControlProduto_Click(object sender, EventArgs e)
        {

        }
    }
}
