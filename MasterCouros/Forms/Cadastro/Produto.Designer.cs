﻿namespace MasterCouros.Forms.Cadastro
{
    partial class Produto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Produto));
            this.TbControlProduto = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel1 = new DevComponents.DotNetBar.TabControlPanel();
            this.gpInfoProduto = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btnRefresh = new DevComponents.DotNetBar.ButtonX();
            this.lblPesquisaProduto = new DevComponents.DotNetBar.LabelX();
            this.txtPesquisaProduto = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnPesquisar = new DevComponents.DotNetBar.ButtonX();
            this.btnRefreshGrid = new DevComponents.DotNetBar.ButtonX();
            this.dgProduto = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btnCancelar = new DevComponents.DotNetBar.ButtonX();
            this.btnSalvar = new DevComponents.DotNetBar.ButtonX();
            this.btnSair = new DevComponents.DotNetBar.ButtonX();
            this.cbBloqueiaProduto = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.txtObs = new DevComponents.DotNetBar.Controls.RichTextBoxEx();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.txtCustoMedio = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.cbBloqueiaCompra = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.txtEstoqueAtual = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.cbControlaEstoque = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.txtUnidVenda = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.txtUnidCompra = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.txtDestino = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.txtGrupo = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.txtDescProduto = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.txtCodProduto = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.tabItemInfoProduto = new DevComponents.DotNetBar.TabItem(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.TbControlProduto)).BeginInit();
            this.TbControlProduto.SuspendLayout();
            this.tabControlPanel1.SuspendLayout();
            this.gpInfoProduto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProduto)).BeginInit();
            this.groupPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TbControlProduto
            // 
            this.TbControlProduto.BackColor = System.Drawing.Color.Transparent;
            this.TbControlProduto.CanReorderTabs = true;
            this.TbControlProduto.ColorScheme.TabBackground = System.Drawing.Color.White;
            this.TbControlProduto.ColorScheme.TabItemBackgroundColorBlend.AddRange(new DevComponents.DotNetBar.BackgroundColorBlend[] {
            new DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(220)))), ((int)(((byte)(244))))), 0F),
            new DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(210)))), ((int)(((byte)(254))))), 0.45F),
            new DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(191)))), ((int)(((byte)(243))))), 0.45F),
            new DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(204)))), ((int)(((byte)(233))))), 1F)});
            this.TbControlProduto.ColorScheme.TabItemHotBackgroundColorBlend.AddRange(new DevComponents.DotNetBar.BackgroundColorBlend[] {
            new DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(237)))), ((int)(((byte)(255))))), 0F),
            new DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(232)))), ((int)(((byte)(255))))), 0.45F),
            new DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(((int)(((byte)(176)))), ((int)(((byte)(210)))), ((int)(((byte)(255))))), 0.45F),
            new DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(218)))), ((int)(((byte)(255))))), 1F)});
            this.TbControlProduto.ColorScheme.TabItemSelectedBackgroundColorBlend.AddRange(new DevComponents.DotNetBar.BackgroundColorBlend[] {
            new DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(227)))), ((int)(((byte)(217))))), 0F),
            new DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(189)))), ((int)(((byte)(116))))), 0.45F),
            new DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(180)))), ((int)(((byte)(89))))), 0.45F),
            new DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255))))), 1F)});
            this.TbControlProduto.ColorScheme.TabPanelBackground2 = System.Drawing.Color.White;
            this.TbControlProduto.Controls.Add(this.tabControlPanel1);
            this.TbControlProduto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TbControlProduto.Location = new System.Drawing.Point(5, 1);
            this.TbControlProduto.Name = "TbControlProduto";
            this.TbControlProduto.SelectedTabFont = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold);
            this.TbControlProduto.SelectedTabIndex = 0;
            this.TbControlProduto.Size = new System.Drawing.Size(1099, 470);
            this.TbControlProduto.TabIndex = 0;
            this.TbControlProduto.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.TbControlProduto.Tabs.Add(this.tabItemInfoProduto);
            this.TbControlProduto.Text = "tabControl1";
            this.TbControlProduto.Click += new System.EventHandler(this.TbControlProduto_Click);
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.tabControlPanel1.Controls.Add(this.gpInfoProduto);
            this.tabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel1.Location = new System.Drawing.Point(0, 28);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel1.Size = new System.Drawing.Size(1099, 442);
            this.tabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.Transparent;
            this.tabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.Transparent;
            this.tabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.tabControlPanel1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel1.Style.GradientAngle = -90;
            this.tabControlPanel1.TabIndex = 1;
            this.tabControlPanel1.TabItem = this.tabItemInfoProduto;
            // 
            // gpInfoProduto
            // 
            this.gpInfoProduto.CanvasColor = System.Drawing.SystemColors.Control;
            this.gpInfoProduto.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.gpInfoProduto.Controls.Add(this.btnRefresh);
            this.gpInfoProduto.Controls.Add(this.lblPesquisaProduto);
            this.gpInfoProduto.Controls.Add(this.txtPesquisaProduto);
            this.gpInfoProduto.Controls.Add(this.btnPesquisar);
            this.gpInfoProduto.Controls.Add(this.btnRefreshGrid);
            this.gpInfoProduto.Controls.Add(this.dgProduto);
            this.gpInfoProduto.Controls.Add(this.groupPanel1);
            this.gpInfoProduto.Controls.Add(this.cbBloqueiaProduto);
            this.gpInfoProduto.Controls.Add(this.txtObs);
            this.gpInfoProduto.Controls.Add(this.labelX9);
            this.gpInfoProduto.Controls.Add(this.txtCustoMedio);
            this.gpInfoProduto.Controls.Add(this.labelX8);
            this.gpInfoProduto.Controls.Add(this.cbBloqueiaCompra);
            this.gpInfoProduto.Controls.Add(this.txtEstoqueAtual);
            this.gpInfoProduto.Controls.Add(this.labelX7);
            this.gpInfoProduto.Controls.Add(this.cbControlaEstoque);
            this.gpInfoProduto.Controls.Add(this.txtUnidVenda);
            this.gpInfoProduto.Controls.Add(this.labelX6);
            this.gpInfoProduto.Controls.Add(this.txtUnidCompra);
            this.gpInfoProduto.Controls.Add(this.labelX5);
            this.gpInfoProduto.Controls.Add(this.txtDestino);
            this.gpInfoProduto.Controls.Add(this.labelX4);
            this.gpInfoProduto.Controls.Add(this.txtGrupo);
            this.gpInfoProduto.Controls.Add(this.labelX3);
            this.gpInfoProduto.Controls.Add(this.txtDescProduto);
            this.gpInfoProduto.Controls.Add(this.labelX2);
            this.gpInfoProduto.Controls.Add(this.txtCodProduto);
            this.gpInfoProduto.Controls.Add(this.labelX1);
            this.gpInfoProduto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gpInfoProduto.Location = new System.Drawing.Point(1, 1);
            this.gpInfoProduto.Name = "gpInfoProduto";
            this.gpInfoProduto.Size = new System.Drawing.Size(1097, 440);
            // 
            // 
            // 
            this.gpInfoProduto.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.gpInfoProduto.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.gpInfoProduto.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.gpInfoProduto.TabIndex = 0;
            // 
            // btnRefresh
            // 
            this.btnRefresh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnRefresh.BackColor = System.Drawing.Color.Transparent;
            this.btnRefresh.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue;
            this.btnRefresh.Location = new System.Drawing.Point(236, 100);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(28, 23);
            this.btnRefresh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnRefresh.Symbol = "";
            this.btnRefresh.TabIndex = 24;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // lblPesquisaProduto
            // 
            this.lblPesquisaProduto.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblPesquisaProduto.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblPesquisaProduto.Location = new System.Drawing.Point(679, 6);
            this.lblPesquisaProduto.Name = "lblPesquisaProduto";
            this.lblPesquisaProduto.Size = new System.Drawing.Size(68, 23);
            this.lblPesquisaProduto.TabIndex = 23;
            this.lblPesquisaProduto.Text = "Descrição:";
            this.lblPesquisaProduto.Visible = false;
            this.lblPesquisaProduto.Click += new System.EventHandler(this.lblPesquisaProduto_Click);
            // 
            // txtPesquisaProduto
            // 
            // 
            // 
            // 
            this.txtPesquisaProduto.Border.Class = "TextBoxBorder";
            this.txtPesquisaProduto.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtPesquisaProduto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPesquisaProduto.Location = new System.Drawing.Point(753, 4);
            this.txtPesquisaProduto.Name = "txtPesquisaProduto";
            this.txtPesquisaProduto.Size = new System.Drawing.Size(341, 25);
            this.txtPesquisaProduto.TabIndex = 22;
            this.txtPesquisaProduto.Visible = false;
            this.txtPesquisaProduto.TextChanged += new System.EventHandler(this.txtPesquisaProduto_TextChanged);
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnPesquisar.AutoCheckOnClick = true;
            this.btnPesquisar.ColorTable = DevComponents.DotNetBar.eButtonColor.Magenta;
            this.btnPesquisar.Location = new System.Drawing.Point(619, 3);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(33, 23);
            this.btnPesquisar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnPesquisar.Symbol = "";
            this.btnPesquisar.TabIndex = 21;
            this.btnPesquisar.CheckedChanged += new System.EventHandler(this.btnPesquisar_CheckedChanged);
            // 
            // btnRefreshGrid
            // 
            this.btnRefreshGrid.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnRefreshGrid.ColorTable = DevComponents.DotNetBar.eButtonColor.Magenta;
            this.btnRefreshGrid.Location = new System.Drawing.Point(581, 3);
            this.btnRefreshGrid.Name = "btnRefreshGrid";
            this.btnRefreshGrid.Size = new System.Drawing.Size(32, 23);
            this.btnRefreshGrid.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnRefreshGrid.Symbol = "";
            this.btnRefreshGrid.TabIndex = 20;
            this.btnRefreshGrid.Click += new System.EventHandler(this.btnRefreshGrid_Click);
            // 
            // dgProduto
            // 
            this.dgProduto.AllowUserToAddRows = false;
            this.dgProduto.AllowUserToDeleteRows = false;
            this.dgProduto.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dgProduto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgProduto.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgProduto.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgProduto.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgProduto.Location = new System.Drawing.Point(581, 32);
            this.dgProduto.Name = "dgProduto";
            this.dgProduto.ReadOnly = true;
            this.dgProduto.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgProduto.Size = new System.Drawing.Size(516, 369);
            this.dgProduto.TabIndex = 8;
            this.dgProduto.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgProduto_CellClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Codigo";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Descrição do Produto";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 350;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "bloqueiacompra";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Visible = false;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "controlaestoque";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Visible = false;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "codgrupo";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Visible = false;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "descgrupo";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Visible = false;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "unidcompra";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Visible = false;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "unidvenda";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Visible = false;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "destino";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Visible = false;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "obs";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Visible = false;
            // 
            // groupPanel1
            // 
            this.groupPanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.btnCancelar);
            this.groupPanel1.Controls.Add(this.btnSalvar);
            this.groupPanel1.Controls.Add(this.btnSair);
            this.groupPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupPanel1.Location = new System.Drawing.Point(0, 388);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(1097, 52);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 13;
            this.groupPanel1.Text = "Ações";
            // 
            // btnCancelar
            // 
            this.btnCancelar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancelar.BackColor = System.Drawing.Color.Transparent;
            this.btnCancelar.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue;
            this.btnCancelar.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnCancelar.Location = new System.Drawing.Point(86, 0);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(91, 26);
            this.btnCancelar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCancelar.Symbol = "";
            this.btnCancelar.TabIndex = 1;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSalvar.BackColor = System.Drawing.Color.Transparent;
            this.btnSalvar.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue;
            this.btnSalvar.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnSalvar.Location = new System.Drawing.Point(0, 0);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(86, 26);
            this.btnSalvar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSalvar.Symbol = "";
            this.btnSalvar.TabIndex = 0;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // btnSair
            // 
            this.btnSair.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSair.BackColor = System.Drawing.Color.Transparent;
            this.btnSair.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue;
            this.btnSair.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSair.Image = ((System.Drawing.Image)(resources.GetObject("btnSair.Image")));
            this.btnSair.Location = new System.Drawing.Point(1023, 0);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(68, 26);
            this.btnSair.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSair.Symbol = "";
            this.btnSair.TabIndex = 2;
            this.btnSair.Text = "Sair";
            this.btnSair.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Left;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // cbBloqueiaProduto
            // 
            this.cbBloqueiaProduto.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.cbBloqueiaProduto.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.cbBloqueiaProduto.Enabled = false;
            this.cbBloqueiaProduto.Location = new System.Drawing.Point(294, 231);
            this.cbBloqueiaProduto.Name = "cbBloqueiaProduto";
            this.cbBloqueiaProduto.Size = new System.Drawing.Size(166, 23);
            this.cbBloqueiaProduto.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbBloqueiaProduto.TabIndex = 11;
            this.cbBloqueiaProduto.Text = "Bloquear Produto";
            // 
            // txtObs
            // 
            // 
            // 
            // 
            this.txtObs.BackgroundStyle.Class = "RichTextBoxBorder";
            this.txtObs.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtObs.Location = new System.Drawing.Point(31, 292);
            this.txtObs.Name = "txtObs";
            this.txtObs.Size = new System.Drawing.Size(506, 109);
            this.txtObs.TabIndex = 12;
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Location = new System.Drawing.Point(31, 263);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(100, 23);
            this.labelX9.TabIndex = 19;
            this.labelX9.Text = "Observações:";
            // 
            // txtCustoMedio
            // 
            // 
            // 
            // 
            this.txtCustoMedio.Border.Class = "TextBoxBorder";
            this.txtCustoMedio.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtCustoMedio.Enabled = false;
            this.txtCustoMedio.Location = new System.Drawing.Point(160, 231);
            this.txtCustoMedio.Name = "txtCustoMedio";
            this.txtCustoMedio.Size = new System.Drawing.Size(100, 25);
            this.txtCustoMedio.TabIndex = 10;
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Location = new System.Drawing.Point(160, 202);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(100, 23);
            this.labelX8.TabIndex = 17;
            this.labelX8.Text = "Custo Médio:";
            // 
            // cbBloqueiaCompra
            // 
            this.cbBloqueiaCompra.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.cbBloqueiaCompra.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.cbBloqueiaCompra.Enabled = false;
            this.cbBloqueiaCompra.Location = new System.Drawing.Point(431, 166);
            this.cbBloqueiaCompra.Name = "cbBloqueiaCompra";
            this.cbBloqueiaCompra.Size = new System.Drawing.Size(136, 23);
            this.cbBloqueiaCompra.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbBloqueiaCompra.TabIndex = 8;
            this.cbBloqueiaCompra.Text = "Bloqueia Compra?";
            // 
            // txtEstoqueAtual
            // 
            // 
            // 
            // 
            this.txtEstoqueAtual.Border.Class = "TextBoxBorder";
            this.txtEstoqueAtual.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtEstoqueAtual.Enabled = false;
            this.txtEstoqueAtual.Location = new System.Drawing.Point(31, 231);
            this.txtEstoqueAtual.Name = "txtEstoqueAtual";
            this.txtEstoqueAtual.Size = new System.Drawing.Size(100, 25);
            this.txtEstoqueAtual.TabIndex = 9;
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Location = new System.Drawing.Point(31, 202);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(100, 23);
            this.labelX7.TabIndex = 14;
            this.labelX7.Text = "Estoque Atual:";
            // 
            // cbControlaEstoque
            // 
            this.cbControlaEstoque.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.cbControlaEstoque.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.cbControlaEstoque.Enabled = false;
            this.cbControlaEstoque.Location = new System.Drawing.Point(294, 166);
            this.cbControlaEstoque.Name = "cbControlaEstoque";
            this.cbControlaEstoque.Size = new System.Drawing.Size(166, 23);
            this.cbControlaEstoque.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbControlaEstoque.TabIndex = 7;
            this.cbControlaEstoque.Text = "Controla Estoque?";
            // 
            // txtUnidVenda
            // 
            // 
            // 
            // 
            this.txtUnidVenda.Border.Class = "TextBoxBorder";
            this.txtUnidVenda.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtUnidVenda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUnidVenda.Enabled = false;
            this.txtUnidVenda.Location = new System.Drawing.Point(160, 166);
            this.txtUnidVenda.Name = "txtUnidVenda";
            this.txtUnidVenda.Size = new System.Drawing.Size(100, 25);
            this.txtUnidVenda.TabIndex = 6;
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Location = new System.Drawing.Point(160, 137);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(100, 23);
            this.labelX6.TabIndex = 11;
            this.labelX6.Text = "Unid Venda:";
            // 
            // txtUnidCompra
            // 
            // 
            // 
            // 
            this.txtUnidCompra.Border.Class = "TextBoxBorder";
            this.txtUnidCompra.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtUnidCompra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUnidCompra.Enabled = false;
            this.txtUnidCompra.Location = new System.Drawing.Point(31, 166);
            this.txtUnidCompra.Name = "txtUnidCompra";
            this.txtUnidCompra.Size = new System.Drawing.Size(100, 25);
            this.txtUnidCompra.TabIndex = 5;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Location = new System.Drawing.Point(31, 137);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(100, 23);
            this.labelX5.TabIndex = 9;
            this.labelX5.Text = "Unid Compra:";
            // 
            // txtDestino
            // 
            this.txtDestino.DisplayMember = "Text";
            this.txtDestino.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.txtDestino.FormattingEnabled = true;
            this.txtDestino.ItemHeight = 19;
            this.txtDestino.Items.AddRange(new object[] {
            this.comboItem1,
            this.comboItem2,
            this.comboItem3});
            this.txtDestino.Location = new System.Drawing.Point(314, 99);
            this.txtDestino.Name = "txtDestino";
            this.txtDestino.Size = new System.Drawing.Size(224, 25);
            this.txtDestino.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.txtDestino.TabIndex = 4;
            this.txtDestino.SelectedValueChanged += new System.EventHandler(this.txtDestino_SelectedValueChanged);
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "COMPRA";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "VENDA";
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "AMBOS";
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Location = new System.Drawing.Point(313, 70);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(131, 23);
            this.labelX4.TabIndex = 6;
            this.labelX4.Text = "Destino:";
            // 
            // txtGrupo
            // 
            this.txtGrupo.DisplayMember = "Text";
            this.txtGrupo.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.txtGrupo.FormattingEnabled = true;
            this.txtGrupo.ItemHeight = 19;
            this.txtGrupo.Location = new System.Drawing.Point(31, 99);
            this.txtGrupo.Name = "txtGrupo";
            this.txtGrupo.Size = new System.Drawing.Size(204, 25);
            this.txtGrupo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.txtGrupo.TabIndex = 3;
            this.txtGrupo.Click += new System.EventHandler(this.txtGrupo_Click);
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Location = new System.Drawing.Point(31, 70);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(75, 23);
            this.labelX3.TabIndex = 4;
            this.labelX3.Text = "Grupo:";
            // 
            // txtDescProduto
            // 
            // 
            // 
            // 
            this.txtDescProduto.Border.Class = "TextBoxBorder";
            this.txtDescProduto.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDescProduto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescProduto.Location = new System.Drawing.Point(160, 40);
            this.txtDescProduto.Name = "txtDescProduto";
            this.txtDescProduto.Size = new System.Drawing.Size(333, 25);
            this.txtDescProduto.TabIndex = 2;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(160, 11);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(75, 23);
            this.labelX2.TabIndex = 2;
            this.labelX2.Text = "Descrição:";
            // 
            // txtCodProduto
            // 
            // 
            // 
            // 
            this.txtCodProduto.Border.Class = "TextBoxBorder";
            this.txtCodProduto.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtCodProduto.Enabled = false;
            this.txtCodProduto.Location = new System.Drawing.Point(31, 40);
            this.txtCodProduto.Name = "txtCodProduto";
            this.txtCodProduto.Size = new System.Drawing.Size(100, 25);
            this.txtCodProduto.TabIndex = 1;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(31, 11);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 23);
            this.labelX1.TabIndex = 0;
            this.labelX1.Text = "Codigo:";
            // 
            // tabItemInfoProduto
            // 
            this.tabItemInfoProduto.AttachedControl = this.tabControlPanel1;
            this.tabItemInfoProduto.BackColor = System.Drawing.Color.Transparent;
            this.tabItemInfoProduto.BackColor2 = System.Drawing.Color.Transparent;
            this.tabItemInfoProduto.Name = "tabItemInfoProduto";
            this.tabItemInfoProduto.Text = "Informações de Produtos";
            // 
            // Produto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1109, 473);
            this.Controls.Add(this.TbControlProduto);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Produto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Produto";
            this.Load += new System.EventHandler(this.Produto_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TbControlProduto)).EndInit();
            this.TbControlProduto.ResumeLayout(false);
            this.tabControlPanel1.ResumeLayout(false);
            this.gpInfoProduto.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgProduto)).EndInit();
            this.groupPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.TabControl TbControlProduto;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel1;
        private DevComponents.DotNetBar.TabItem tabItemInfoProduto;
        private DevComponents.DotNetBar.Controls.GroupPanel gpInfoProduto;
        private DevComponents.DotNetBar.Controls.ComboBoxEx txtGrupo;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDescProduto;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX txtCodProduto;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx txtDestino;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.Editors.ComboItem comboItem3;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.TextBoxX txtUnidVenda;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.Controls.TextBoxX txtUnidCompra;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgProduto;
        private DevComponents.DotNetBar.Controls.CheckBoxX cbBloqueiaProduto;
        private DevComponents.DotNetBar.Controls.RichTextBoxEx txtObs;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.Controls.TextBoxX txtCustoMedio;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.Controls.CheckBoxX cbBloqueiaCompra;
        private DevComponents.DotNetBar.Controls.TextBoxX txtEstoqueAtual;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.Controls.CheckBoxX cbControlaEstoque;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.ButtonX btnCancelar;
        private DevComponents.DotNetBar.ButtonX btnSalvar;
        private DevComponents.DotNetBar.ButtonX btnSair;
        private DevComponents.DotNetBar.ButtonX btnRefreshGrid;
        private DevComponents.DotNetBar.ButtonX btnPesquisar;
        private DevComponents.DotNetBar.LabelX lblPesquisaProduto;
        private DevComponents.DotNetBar.Controls.TextBoxX txtPesquisaProduto;
        private DevComponents.DotNetBar.ButtonX btnRefresh;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
    }
}