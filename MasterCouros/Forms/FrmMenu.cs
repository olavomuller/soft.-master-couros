﻿using MasterCouros.Forms.Cadastro;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MasterCouros.Forms
{
    public partial class FrmMenu : DevComponents.DotNetBar.Office2007RibbonForm
    {
        public FrmMenu()
        {
            InitializeComponent();
        }

        private void FrmMenu_Load(object sender, EventArgs e)
        {
            //Mudar a cor de fundo
            foreach (Control ctl in this.Controls)
            {
                if (ctl is MdiClient)
                {
                    MdiClient ctlMDI = (MdiClient)ctl;
                    ctlMDI.BackColor = this.BackColor;
                    break;
                }
            }

        }


                
        private void buttonItem14_Click(object sender, EventArgs e)
        {
            Form1 meuform = new Form1();
            meuform.MdiParent = this;
            meuform.Show();
        }

        private void btnOrcamento_Click(object sender, EventArgs e)
        {
           
        }

       

       

        private void btnCadGrupoProduto_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            GrupoProdutos meuform = new GrupoProdutos();
            meuform.MdiParent = this;
            meuform.Show();
            Cursor = Cursors.Default;
        }

        private void btnCadProduto_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            Produto meuform = new Produto();
            meuform.MdiParent = this;
            meuform.Show();
            Cursor = Cursors.Default;
        }
    }
}
