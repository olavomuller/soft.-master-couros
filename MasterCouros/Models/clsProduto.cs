﻿using System;
using System.Windows.Forms;


namespace MasterCouros.Models
{
    public class clsProduto
    {
        #region Propriedades
        public int CodProduto { get; set; }
        public string DescProduto { get; set; }
        public int CodGrupo { get; set; }
        public decimal EstoqueAtual { get; set; }
        public decimal VlCustoMedio { get; set; }
        public string BloqueiaCompra { get; set; }
        public string ControlaEstoque { get; set; }
        public string UnidCompra { get; set; }
        public string UnidVenda { get; set; }
        public string Destino { get; set; }
        public string BloqueiaProduto { get; set; }
        public string Obs { get; set; }
        private clsDAL admbanco;
        public DataGridView DgVirtual;
        #endregion

        #region Comandos
        public void InserirProduto()
        {
            admbanco = new clsDAL();
            admbanco.Insert("insert into produto(descproduto, codgrupo, bloqueiacompra, controlaestoque, unidcompra, unidvenda, destino, obs, bloqueiaproduto) values ('"+ DescProduto +"', '"+ CodGrupo +"', '"+ BloqueiaCompra +"', '"+ ControlaEstoque +"', '"+ UnidCompra +"', '"+ UnidVenda +"', '"+ Destino +"', '"+ Obs +"', '"+ BloqueiaProduto +"')");
            
        }

        public void AlterarProduto()
        {
            admbanco = new clsDAL();
            //admbanco.Update("update produto set descgrupo = '"+ DescProduto +"', 
                //");
        }
        #endregion

        #region Consultas
        public void ListaProdutos()
        {
            admbanco = new clsDAL();
            DgVirtual = new DataGridView();

            try
            {

                DgVirtual.Columns.Add("codprod", "codprod");
                DgVirtual.Columns.Add("descprod", "descprod");
                DgVirtual.Columns.Add("bloqueiacompra", "bloqueiacompra");
                DgVirtual.Columns.Add("controlaestoque", "controlaestoque");
                DgVirtual.Columns.Add("codgrupo", "codgrupo");
                DgVirtual.Columns.Add("descgrupo", "descgrupo");
                DgVirtual.Columns.Add("unidcompra", "unidcompra");
                DgVirtual.Columns.Add("unidvenda", "unidvenda");
                DgVirtual.Columns.Add("destino", "destino");
                DgVirtual.Columns.Add("obs", "obs");

                DgVirtual.AllowUserToAddRows = false;

                admbanco.PreencherReader("select p.codproduto, p.descproduto, p.bloqueiacompra, gp.codgrupoproduto, gp.descgrupo, " +
                    "p.controlaestoque, p.unidcompra, p.unidvenda, p.destino, p.obs" + 
                    " from produto p, grupoproduto gp where p.codgrupo = gp.codgrupoproduto" +
                    " and p.bloqueiaproduto = ''" +
                    " and gp.desativagrupo = '' order by p.descproduto");

                while (admbanco.MyDr.Read())
                {

                    DgVirtual.Rows.Add(admbanco.MyDr["codproduto"].ToString(), admbanco.MyDr["descproduto"].ToString()
                        , admbanco.MyDr["bloqueiacompra"].ToString(), admbanco.MyDr["controlaestoque"].ToString()
                        , admbanco.MyDr["codgrupoproduto"].ToString(), admbanco.MyDr["descgrupo"].ToString()
                        , admbanco.MyDr["unidcompra"].ToString(), admbanco.MyDr["unidvenda"].ToString()
                        , admbanco.MyDr["destino"].ToString(), admbanco.MyDr["obs"].ToString());
                }

                if (admbanco.MyDr.HasRows == false)
                {
                    admbanco.MyDr.Close();
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("erro: " + ex.Message);
            }
        }

        public void ListaProdutosByDescricao()
        {
            admbanco = new clsDAL();
            DgVirtual = new DataGridView();

            try
            {

                DgVirtual.Columns.Add("codprod", "codprod");
                DgVirtual.Columns.Add("descprod", "descprod");
                DgVirtual.Columns.Add("bloqueiacompra", "bloqueiacompra");
                DgVirtual.Columns.Add("controlaestoque", "controlaestoque");
                DgVirtual.Columns.Add("codgrupo", "codgrupo");
                DgVirtual.Columns.Add("descgrupo", "descgrupo");
                DgVirtual.Columns.Add("unidcompra", "unidcompra");
                DgVirtual.Columns.Add("unidvenda", "unidvenda");
                DgVirtual.Columns.Add("destino", "destino");
                DgVirtual.Columns.Add("obs", "obs");

                DgVirtual.AllowUserToAddRows = false;

                admbanco.PreencherReader("select p.codproduto, p.descproduto, p.bloqueiacompra, gp.codgrupoproduto, gp.descgrupo, " +
                    "p.controlaestoque, p.unidcompra, p.unidvenda, p.destino, p.obs" +
                    " from produto p, grupoproduto gp where p.descproduto like '%" + DescProduto + "%' and p.codgrupo = gp.codgrupo" +
                    " and p.bloqueiaproduto = ''" +
                    " and gp.desativagrupo = '' order by p.codproduto");

                while (admbanco.MyDr.Read())
                {
                    
                          DgVirtual.Rows.Add(admbanco.MyDr["codproduto"].ToString(), admbanco.MyDr["descproduto"].ToString()
                        , admbanco.MyDr["bloqueiacompra"].ToString(), admbanco.MyDr["controlaestoque"].ToString()
                        , admbanco.MyDr["codgrupoproduto"].ToString(), admbanco.MyDr["descgrupo"].ToString()
                        , admbanco.MyDr["unidcompra"].ToString(), admbanco.MyDr["unidvenda"].ToString()
                        , admbanco.MyDr["destino"].ToString(), admbanco.MyDr["obs"].ToString());
                }

                if (admbanco.MyDr.HasRows == false)
                {
                    admbanco.MyDr.Close();
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("erro: " + ex.Message);
            }
        }

        #endregion

      

    }
}
