﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Data;

namespace MasterCouros.Models
{
    public class clsDAL
    {
        private MySqlConnection connection;
        private MySqlCommand cmd;
        private string server;
        private string database;
        private string uid;
        private string password;
        public MySqlDataReader MyDr;

        public clsDAL()
        {
            Initialize();
        }

        //Initialize values
        private void Initialize()
        {
            server = "localhost";
            database = "mastercouros";
            uid = "root";
            password = "";

            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

            connection = new MySqlConnection(connectionString);
        }

        //open connection to database
        private bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {

                switch (ex.Number)
                {
                    case 0:
                        MessageBox.Show("Não foi possível se conectar no banco de dados.  Contate o administrador", "ERRO NA CLASSE DAL");
                        break;

                    case 1045:
                        MessageBox.Show("Usuário e/ou senha inválido, por favor tente novamente");
                        break;
                }
                return false;
            }
        }

        //Close connection
        private bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        //Insert statement

        public void Insert(string SQL)
        {
            string query = SQL;
            //open connection
            if (this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                cmd = new MySqlCommand(query, connection);

                //Execute command
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
        }

        //update statement

        public void Update(string SQL)
        {
            string query = SQL;
            //open connection
            if (this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                cmd = new MySqlCommand(query, connection);

                //Execute command
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
        }

        public void PreencherReader(string SQL)
        {
            string query = SQL;
            if (this.OpenConnection() == true)
            {
             
                cmd = new MySqlCommand(query, connection);
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = query;
                MyDr = cmd.ExecuteReader();

                //this.CloseConnection();
            }
        }
    }
}
