﻿using System.Windows.Forms;

namespace MasterCouros.Models
{
    public class clsPessoa
    {
        public string idpessoa { get; set; }
        public string nome { get; set; }
        private clsDAL admbanco { get; set; }
        public System.Windows.Forms.ListView MinhaLista;

        public clsPessoa()
        {
            admbanco = new clsDAL();
            MinhaLista = new System.Windows.Forms.ListView();
        }
        public void InserirPessoa()
        {
            admbanco = new clsDAL();
            admbanco.Insert("insert into pessoa(nome) values ('"+ nome +"')");
            
            
        }

        public void ListaPessoas()
        {
            admbanco = new clsDAL();
            try
            {
                
                MinhaLista.Columns.Add("nome", "nome");
                admbanco.PreencherReader("select * from pessoa");
                while (admbanco.MyDr.Read())
                {
                    MinhaLista.Items.Add(admbanco.MyDr["nome"].ToString());
                }

                if (admbanco.MyDr.HasRows == false)
                {
                    admbanco.MyDr.Close();
                }
            }
            catch
            {
                MessageBox.Show("Erro: ");
            }


        }
    }
}
