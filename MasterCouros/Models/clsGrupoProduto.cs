﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MasterCouros.Models
{
   public class clsGrupoProduto
    {
        public int CodGrupoProduto { get; set; }
        public string DescGrupo { get; set; }
        public string DesativaGrupo { get; set; }
        private clsDAL admbanco;
        public DataGridView DgVirtual;

        public void InserirGrupo()
        {
            admbanco = new clsDAL();
            try
            {
                admbanco.Insert("insert into grupoproduto(descgrupo, desativagrupo) values ('" + DescGrupo + "', '"+ DesativaGrupo +"')");
            }
            catch(Exception e )
            {
                MessageBox.Show("Erro: " + e.Message);
            }
        }

        public void AlterarGrupo()
        {
            admbanco = new clsDAL();
            try
            {
                admbanco.Update("Update grupoproduto set descgrupo = '"+ DescGrupo +"', desativagrupo ='"+ DesativaGrupo +"' where codgrupoproduto = '"+ CodGrupoProduto +"'");
            }
            catch (Exception e)
            {
                MessageBox.Show("Erro: " + e.Message);
            }
        }

        public void ListaGrupo()
        {
            admbanco = new clsDAL();
            DgVirtual = new DataGridView();

            try
            {
                
                DgVirtual.Columns.Add("codgrupo", "codgrupo");
                DgVirtual.Columns.Add("nomegrupo", "nomegrupo");
                DgVirtual.AllowUserToAddRows = false;

                admbanco.PreencherReader("select codgrupoproduto, descgrupo from grupoproduto where desativagrupo is null  or desativagrupo = '' order by descgrupo");
                while(admbanco.MyDr.Read())
                {
                    
                    DgVirtual.Rows.Add(admbanco.MyDr["descgrupo"].ToString(), admbanco.MyDr["codgrupoproduto"].ToString());
                }

                if (admbanco.MyDr.HasRows == false)
                {
                    admbanco.MyDr.Close();
                }
            }

            catch(Exception e)
            {
                MessageBox.Show("erro: " + e.Message );
            }
           
        }
    }
}
