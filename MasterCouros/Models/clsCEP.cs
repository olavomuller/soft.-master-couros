﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MasterCouros.Models
{
   public class clsCEP
    {
        private string CaminhoWs = "http://cep.republicavirtual.com.br/web_cep.php?cep=@cep&formato=xml";
        private DataSet ds;
        private string resultado;
        private string resultado_txt;

        public string Cep { get; set; }
        public string Endereco { get; set;}
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }


        public void EncontrarCep()
        {
            try
            {
                ds = new DataSet();
                ds.ReadXml(CaminhoWs.Replace("@cep", Cep));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    resultado = ds.Tables[0].Rows[0]["resultado"].ToString();
                    switch(resultado)
                    {
                        case "1":
                            Estado = ds.Tables[0].Rows[0]["uf"].ToString().Trim();
                            Cidade = ds.Tables[0].Rows[0]["cidade"].ToString().Trim();
                            Bairro = ds.Tables[0].Rows[0]["bairro"].ToString().Trim();
                            Endereco = ds.Tables[0].Rows[0]["tipo_logradouro"].ToString().Trim() + 
                                " " + ds.Tables[0].Rows[0]["logradouro"].ToString().Trim();
                            resultado_txt = "CEP completo";
                            break;
                        case "2":
                            Estado = ds.Tables[0].Rows[0]["uf"].ToString().Trim();
                            Cidade = ds.Tables[0].Rows[0]["cidade"].ToString().Trim();
                            resultado_txt = "CEP único";
                            break;
                        default:
                            resultado_txt = "CEP não encontrado";
                            break;
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Erro na Classe de CEP: " + ex.Message);
            }
        }
    }
}
